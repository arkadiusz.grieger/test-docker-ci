FROM registry.gitlab.com/arkadiusz.grieger/nginx-php:php-8.1
ADD . /var/www
RUN COMPOSER_MEMORY_LIMIT=-1 composer install --ignore-platform-reqs
